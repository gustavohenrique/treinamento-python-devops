# Treinamento Python para DevOps

## Desafio
Dado um nome de usuário fornecido como parâmetro, o sistema deve obter os repositórios dele do Github e criar um arquivo CSV contendo nome do usuário, nome do repositório, url, linguagem e número de estrelas. 
Ex.: https://api.github.com/users/gustavohenrique/repos

## Snippets

### Converter uma String JSON para um dicionario
```
import json
json_str = '{"chave3": 3, "chave4": 4}'
json_dict = json.loads(json_str)
```

### Fazer um request HTTP e converter o JSON do response
```
from urllib import request, parse
import json

req = request.Request('url', headers={})
with request.urlopen(req) as response:
    resp = response.read()
    conteudo = json.loads(resp)
```

### Criar um arquivo CSV
```
import csv

linha1 = ['coluna 1', 'coluna 2']
linha2 = ['outra coluna 1', 'outra coluna 2']
conteudo = [linha1, linha2]
with open('/tmp/arquivocsv', 'w') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerows(conteudo)
```
  
